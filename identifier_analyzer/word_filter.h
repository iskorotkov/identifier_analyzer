#pragma once
#include <string>

/** ����� ��� ������� ���� � �������� */
class word_filter
{
public:
	/**
	 * ����������, �������� �� ����� ��� �������� ������
	 *
	 * ��������: ����� ����� � ���������� [0;256] � �������� ���������� ��������������� ����� C++, �. �. ���������� � ����� ��������, ��������� ��� ������� _, � ��� ������ ������� �������� ������� ��������, ���������, ��������� _ ��� �������
	 */
	bool is_valid_word(const std::string& word) const;

	/** ���������, �������� �� ������ ���������� ������ �������� �������������� C++ */
	bool is_valid_first_character(char c) const;

	/** ���������, �������� �� ������ ���������� �������� �������� �������������� C++ */
	bool is_valid_character(char c) const;

	/** ���������, ����� ����� ���� � ���������� [0;256] */
	bool is_valid_length(std::string word) const;
};
